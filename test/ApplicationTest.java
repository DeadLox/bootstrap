import org.junit.Test;
import play.Logger;
import play.mvc.Http;
import play.test.FunctionalTest;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by cleborgne on 08/08/2014.
 */
public class ApplicationTest extends FunctionalTest {

    @Test
    public void testHomePage() {
        Http.Response response = GET("/");
        assertIsOk(response);
        assertContentType("text/html", response);
        assertCharset(play.Play.defaultWebEncoding, response);
    }

    @Test
    public void testPhotosPage() {
        Map<String, String> params = new HashMap<String, String>();
        params.put("email", "dead.lox@hotmail.fr");
        params.put("password", "admin");
        Http.Response response = POST("/login", params);

        // Le vrai test avec cookies
        final Http.Request request = newRequest();
        request.path = "/photos";
        request.url  = "/photos";
        request.cookies = response.cookies;
        request.method = "GET";
        final Http.Response responseTest = makeRequest(request);
        assertIsOk(responseTest);
        assertContentType("text/html", responseTest);
        assertCharset(play.Play.defaultWebEncoding, responseTest);
    }

    @Test
    public void testClassementsPage() {
        Http.Response response = GET("/classements");
        assertIsOk(response);
        assertContentType("text/html", response);
        assertCharset(play.Play.defaultWebEncoding, response);
    }

    @Test
    public void testMapPage() {
        Http.Response response = GET("/map");
        assertIsOk(response);
        assertContentType("text/html", response);
        assertCharset(play.Play.defaultWebEncoding, response);
    }
}
