package fixtures;

import models.Configuration;
import models.Utilisateur;
import play.Logger;
import play.jobs.Job;
import play.jobs.OnApplicationStart;
import play.test.Fixtures;

/**
 * Created by DeadLox on 02/08/2014.
 */
@OnApplicationStart
public class Loader extends Job {

    public void doJob() {
        if (Configuration.count() == 0) {
            play.Logger.warn("Load configuration");
            Fixtures.loadModels("init-conf.yml");
        }
        if (Utilisateur.count() == 0) {
            Logger.warn("Load fixtures");
            Fixtures.loadModels("init-data.yml");
        }
    }
}
