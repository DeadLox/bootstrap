package controllers;

import models.Article;
import models.Category;
import org.json.JSONArray;
import org.json.JSONObject;
import play.*;
import play.i18n.Messages;
import play.mvc.*;
import util.CategoryUtil;

import java.util.*;

@With(Maintenance.class)
public class Application extends BootstrapController {
    private static String CAT_CLASSEMENTS = "Classements";
    private static String CAT_PHOTOS = "Photos";

    public static void index() {
        render();
    }

    public static void classements() {
        render();
    }

    public static void photos() throws Throwable {
        if (!Security.isConnected()) {
            flash.error(Messages.get("secure.access.denied"));
            Secure.login();
        }
        render();
    }

    /**
     * Retourne les contenus d'une catégorie au format JSON
     * @param id L'id de la catégorie
     */
    public static void getClassements(long id){
        JSONObject jsonObject = new JSONObject();
        Category category = null;
        if (id != 0) {
            category = Category.findById(id);
        } else {
            category = Category.find("byTitle", CAT_CLASSEMENTS).first();
        }
        if (category != null) {
            jsonObject = CategoryUtil.fetchCategory(category);
        }
        renderJSON(jsonObject.toString());
    }

    /**
     * Retourne les contenus d'une catégorie au format JSON
     * @param id L'id de la catégorie
     */
    public static void getPhotos(long id){
        JSONObject jsonObject = new JSONObject();
        Category category = null;
        if (id != 0) {
            category = Category.findById(id);
        } else {
            category = Category.find("byTitle", CAT_PHOTOS).first();
        }
        if (category != null) {
            jsonObject = CategoryUtil.fetchCategory(category);
        }
        renderJSON(jsonObject.toString());
    }

    public static void map(){
        render();
    }

}